# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-11-14 01:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: ./README.md:1 ./examples/README.md:1 ./examples/sample.md:1 ./TODO.md:1
#, no-wrap
msgid "---\n"
msgstr ""

#. type: Plain text
#: ./README.md:5
#, no-wrap
msgid ""
"title: Onion Reveal\n"
"subtitle: A static site generator using reveal.js with a Tor theme\n"
"author: Community Team - The Tor Project\n"
"---\n"
msgstr ""

#. type: Plain text
#: ./README.md:8
msgid "## Onion Reveal"
msgstr ""

#. type: Plain text
#: ./README.md:10
msgid "This is a [reveal.js][] slides compiler similar to [Onion TeX Slim][]."
msgstr ""

#. type: Plain text
#: ./README.md:12
msgid "It was written during a [Tor Hackweek project][]."
msgstr ""

#. type: Plain text
#: ./README.md:14
msgid "[reveal.js]: https://revealjs.com"
msgstr ""

#. type: Plain text
#: ./README.md:16
msgid "[Onion TeX Slim]: https://gitlab.torproject.org/rhatto/onion-tex-slim"
msgstr ""

#. type: Plain text
#: ./README.md:18
msgid ""
"[Tor Hackweek project]: "
"https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15"
msgstr ""

#. type: Plain text
#: ./README.md:20 ./TODO.md:23
msgid "## Features"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:24
#, markdown-text
msgid "Builds web-based slideshows with Tor theming."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:24
#, markdown-text
msgid "Modular and non-intrusive: can be included within any project."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:24
#, markdown-text
msgid "Localization support through [gettext][]."
msgstr ""

#. type: Plain text
#: ./README.md:26
msgid ""
"[gettext]: "
"https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html"
msgstr ""

#. type: Plain text
#: ./README.md:28
msgid "## How it works"
msgstr ""

#. type: Plain text
#: ./README.md:30
msgid "This software could works like this:"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:34
#, markdown-text
msgid ""
"Uses a Makefile/script target that traverses a folder looking for `.md` "
"files."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:34
#, markdown-text
msgid "Then it builds the HTML slides using a [Pandoc][] [reveal.js][] template."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:34
#, markdown-text
msgid ""
"With CI/CD for automation, in a way that's easy to integrate into existing "
"projects."
msgstr ""

#. type: Plain text
#: ./README.md:36
msgid "[Pandoc]: https://pandoc.org"
msgstr ""

#. type: Plain text
#: ./README.md:38
msgid "## Examples"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:41
#, markdown-text
msgid ""
"[Onion Reveal's "
"website](https://tpo.pages.torproject.net/web/onion-reveal/)."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:41
#, markdown-text
msgid "[Sample presentation](examples/sample.md)."
msgstr ""

#. type: Plain text
#: ./README.md:43
msgid "## Repository"
msgstr ""

#. type: Plain text
#: ./README.md:45
msgid ""
"Onion Reveal's repository is hosted at "
"[https://gitlab.torproject.org/tpo/web/onion-reveal/](https://gitlab.torproject.org/tpo/web/onion-reveal/)."
msgstr ""

#. type: Plain text
#: ./README.md:47
msgid "## Requirements"
msgstr ""

#. type: Plain text
#: ./README.md:49
msgid "Onion Reveal requires the following software:"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:55
#, markdown-text
msgid "[Git](https://git-scm.com)."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:55
#, markdown-text
msgid "[NPM](https://www.npmjs.com)."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:55
#, markdown-text
msgid "[Gulp](https://gulpjs.com)."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:55
#, markdown-text
msgid "[Pandoc](https://pandoc.org)."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:55
#, markdown-text
msgid "[rsync](https://rsync.samba.org)."
msgstr ""

#. type: Plain text
#: ./README.md:57
msgid ""
"An example installation script tested on [Debian][] Bookworm is available at "
"[scripts/onion-reveal-provision-build][]."
msgstr ""

#. type: Plain text
#: ./README.md:62
msgid ""
"[Debian]: https://www.debian.org [texlive-latex-extra]: "
"https://tracker.debian.org/pkg/texlive-extra "
"[scripts/onion-reveal-provision-build]: scripts/onion-reveal-provision-build "
"[po4a]: https://po4a.org"
msgstr ""

#. type: Plain text
#: ./README.md:64
msgid "## Optional dependencies"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:67
#, markdown-text
msgid ""
"[GNU Make](https://www.gnu.org/software/make) for building through "
"Makefiles."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:67
#, markdown-text
msgid "[po4a][] to handle translations."
msgstr ""

#. type: Plain text
#: ./README.md:69
msgid "## Installation"
msgstr ""

#. type: Plain text
#: ./README.md:72
msgid ""
"This repository is ready to be used in any existing repository.  It can be "
"installed in a number of ways as follows."
msgstr ""

#. type: Plain text
#: ./README.md:74
msgid "## Forking the project"
msgstr ""

#. type: Plain text
#: ./README.md:76
msgid "Simply fork this project and change whatever you need."
msgstr ""

#. type: Plain text
#: ./README.md:78
msgid "## As a Git submodule"
msgstr ""

#. type: Plain text
#: ./README.md:80
msgid ""
"You can simply put it as a Git submodule somewhere, like in a `vendors` "
"folder of your project:"
msgstr ""

#. type: Plain text
#: ./README.md:84
#, no-wrap
msgid ""
"    mkdir -p vendor && \\\n"
"    git submodule add --depth 1 \\\n"
"      https://gitlab.torproject.org/tpo/web/onion-reveal "
"vendors/onion-reveal\n"
msgstr ""

#. type: Plain text
#: ./README.md:86
msgid "You also need to initialize Onion Reveal's submodules:"
msgstr ""

#. type: Plain text
#: ./README.md:88
#, no-wrap
msgid "    git -C vendors/onion-reveal submodule update --init --depth 1\n"
msgstr ""

#. type: Plain text
#: ./README.md:90
msgid ""
"Then use symbolic links to reference all the needed files from the root of "
"your repository."
msgstr ""

#. type: Plain text
#: ./README.md:92
msgid "## Manually copying"
msgstr ""

#. type: Plain text
#: ./README.md:94
msgid ""
"Even simpler, copy all the relevant files from this repository into your "
"project."
msgstr ""

#. type: Plain text
#: ./README.md:96
msgid "## Outside the project"
msgstr ""

#. type: Plain text
#: ./README.md:100
msgid ""
"The Onion Reveal repository can be installed anywhere in your system.  In "
"fact, it does not need to be stored inside your project if you don't want "
"to.  You just need to make sure to invoke the scripts from your project "
"folder or pass the appropriate folder or file path as arguments to it's "
"scripts."
msgstr ""

#. type: Plain text
#: ./README.md:102
msgid "## Other ways"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:105
#, markdown-text
msgid "`git-subtree(1)`."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:105
#, markdown-text
msgid "?"
msgstr ""

#. type: Plain text
#: ./README.md:107
msgid "## Installation notes"
msgstr ""

#. type: Bullet: '* '
#: ./README.md:111
#, markdown-text
msgid ""
"If you plan to use either GitLab CI/CD or GitHub Actions to build your "
"documentation, make sure to at least copy/adapt the corresponding files from "
"the submodule."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:111
#, markdown-text
msgid ""
"You can't just symlink those since the submodule won't be accessible when "
"GitLab and GitHub looks for CI/CD or Actions files."
msgstr ""

#. type: Bullet: '* '
#: ./README.md:111
#, markdown-text
msgid ""
"The provided CI configuration takes care of initializing the submodule "
"during build time, but they should be available in the main repo in order to "
"do so."
msgstr ""

#. type: Plain text
#: ./README.md:113
msgid "## Project layout"
msgstr ""

#. type: Plain text
#: ./README.md:115
msgid "Example project layout:"
msgstr ""

#. type: Bullet: '    * '
#: ./README.md:125
#, markdown-text
msgid "/path/to/my/project"
msgstr ""

#. type: Bullet: '        * '
#: ./README.md:125
#, markdown-text
msgid "slides"
msgstr ""

#. type: Bullet: '          * '
#: ./README.md:125
#, markdown-text
msgid "README.md"
msgstr ""

#. type: Bullet: '          * '
#: ./README.md:125
#, markdown-text
msgid "slide-for-conference-1"
msgstr ""

#. type: Bullet: '              * '
#: ./README.md:125
#, markdown-text
msgid "slide-for-conference-1.md"
msgstr ""

#. type: Bullet: '          * '
#: ./README.md:125
#, markdown-text
msgid "slide-for-conference-2"
msgstr ""

#. type: Bullet: '              * '
#: ./README.md:125
#, markdown-text
msgid "slide-for-conference-2.md"
msgstr ""

#. type: Bullet: '        * '
#: ./README.md:125
#, markdown-text
msgid "vendors"
msgstr ""

#. type: Bullet: '            * '
#: ./README.md:125
#, markdown-text
msgid "onion-reveal"
msgstr ""

#. type: Plain text
#: ./README.md:127
msgid "## Building the theme"
msgstr ""

#. type: Plain text
#: ./README.md:129
msgid ""
"To build the theme, follow the instructions at "
"[vendors/onion-reveal/vendors/revealjs_tor_theme/README][] or use the "
"following script, if you're using a [Debian][]-like system:"
msgstr ""

#. type: Plain text
#: ./README.md:131
#, no-wrap
msgid "    ./vendors/onion-reveal/scripts/onion-reveal-build-theme\n"
msgstr ""

#. type: Plain text
#: ./README.md:133
msgid "[vendors/onion-reveal/vendors/revealjs_tor_theme/README]:"
msgstr ""

#. type: Plain text
#: ./README.md:135
msgid "## Compiling all slides"
msgstr ""

#. type: Plain text
#: ./README.md:137
msgid ""
"Onion Reveal can be used to build static websites from any set of Markdown "
"files it finds, so it's recommended to run it against your project slides "
"folder."
msgstr ""

#. type: Plain text
#: ./README.md:139
msgid ""
"Once all dependencies are installed and the required files are in place in "
"your repository, just run the build script:"
msgstr ""

#. type: Plain text
#: ./README.md:141
#, no-wrap
msgid "    vendors/onion-reveal/scripts/onion-reveal-build-all slides/\n"
msgstr ""

#. type: Plain text
#: ./README.md:143
msgid ""
"If invoked without arguments, the script above will build slides from all "
"Markdown file it finds recursively from the current folder."
msgstr ""

#. type: Plain text
#: ./README.md:145
msgid "## Compiling a single slide"
msgstr ""

#. type: Plain text
#: ./README.md:147
msgid "You can also build a single slide deck:"
msgstr ""

#. type: Plain text
#: ./README.md:150
#, no-wrap
msgid ""
"    vendors/onion-reveal/scripts/onion-reveal-build \\\n"
"      slides/slide-for-conference-1/slide-for-conference-1.md\n"
msgstr ""

#. type: Plain text
#: ./README.md:152
msgid "## Translation support"
msgstr ""

#. type: Plain text
#: ./README.md:156
msgid ""
"Onion Reveal plays well with localization (l10n).  Out of the box, it "
"supports translations through [L10n for Markdown][].  If you have [po4a][] "
"installed, you can use it by copying/customizing the provided "
"[l10n.sample][] and running the following script everytime you want to sync "
"your translations:"
msgstr ""

#. type: Plain text
#: ./README.md:158
#, no-wrap
msgid "    ./vendors/l10n-for-markdown/scripts/l10n-for-markdown-update-locales\n"
msgstr ""

#. type: Plain text
#: ./README.md:161
msgid ""
"[L10n for Markdown]: "
"https://gitlab.torproject.org/tpo/community/l10n-for-markdown/ "
"[l10n.sample]: "
"https://gitlab.torproject.org/tpo/community/l10n-for-markdown/-/blob/main/l10n.sample"
msgstr ""

#. type: Plain text
#: ./README.md:163
msgid "## Makefile"
msgstr ""

#. type: Plain text
#: ./README.md:165
msgid ""
"Onion Reveal also comes with a [sample Makefile][] that can be customized to "
"your needs, placed in the desired folder and then used to build all your "
"slides through"
msgstr ""

#. type: Plain text
#: ./README.md:167
#, no-wrap
msgid "    make onion-reveal-build-all\n"
msgstr ""

#. type: Plain text
#: ./README.md:169
msgid "You can also build specific slides using `make`:"
msgstr ""

#. type: Plain text
#: ./README.md:171
#, no-wrap
msgid ""
"    make -C slides "
"slides/slide-for-conference-1/slide-for-conference-1.html\n"
msgstr ""

#. type: Plain text
#: ./README.md:173
msgid ""
"[sample Makefile]: "
"https://gitlab.torproject.org/tpo/community/onion-reveal/-/blob/main/Makefile.onion-reveal"
msgstr ""

#. type: Plain text
#: ./README.md:175
msgid "## Continuous Integration"
msgstr ""

#. type: Plain text
#: ./README.md:178
msgid ""
"Onion Reveal support Continuous Integration (CI) through GitLab.  Check the "
"[.gitlab-ci-onion-reveal.yml][] file for details."
msgstr ""

#. type: Plain text
#: ./README.md:180
msgid "[.gitlab-ci-onion-reveal.yml]: .gitlab-ci-onion-reveal.yml"
msgstr ""

#. type: Plain text
#: ./README.md:182
msgid "## TODO and issues"
msgstr ""

#. type: Plain text
#: ./README.md:184
msgid "Check existing and report new issues in the [ticket queue][]."
msgstr ""

#. type: Plain text
#: ./README.md:185
msgid "[ticket queue]: https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues"
msgstr ""

#. type: Plain text
#: ./examples/README.md:5
#, no-wrap
msgid ""
"title: Onion Reveal Examples\n"
"subtitle: Misc examples\n"
"author: Onion Reveal Team\n"
"---\n"
msgstr ""

#. type: Plain text
#: ./examples/README.md:8
msgid "# Examples"
msgstr ""

#. type: Plain text
#: ./examples/README.md:9
msgid "This folder has sample examples."
msgstr ""

#. type: Plain text
#: ./examples/sample.md:5
#, no-wrap
msgid ""
"title: Onion Reveal Sample\n"
"subtitle: An example\n"
"author: Someone\n"
"---\n"
msgstr ""

#. type: Plain text
#: ./examples/sample.md:8
msgid "# First section"
msgstr ""

#. type: Bullet: '* '
#: ./examples/sample.md:10
#, markdown-text
msgid "An item."
msgstr ""

#. type: Plain text
#: ./examples/sample.md:12
msgid "# Another section"
msgstr ""

#. type: Bullet: '1. '
#: ./examples/sample.md:16
#, markdown-text
msgid "Numbered list."
msgstr ""

#. type: Bullet: '2. '
#: ./examples/sample.md:16
#, markdown-text
msgid "Another item."
msgstr ""

#. type: Bullet: '3. '
#: ./examples/sample.md:16
#, markdown-text
msgid "Another"
msgstr ""

#. type: Plain text
#: ./examples/sample.md:18
msgid "# Codeblocks"
msgstr ""

#. type: Plain text
#: ./examples/sample.md:21
msgid "``` this is a codeblock ```"
msgstr ""

#. type: Plain text
#: ./TODO.md:3
#, no-wrap
msgid ""
"title: TODO - Onion Reveal\n"
"---\n"
msgstr ""

#. type: Plain text
#: ./TODO.md:6
msgid ""
"Tasks not yet moved to the [issue queue][], as sometimes editing a TODO file "
"is way quicker."
msgstr ""

#. type: Plain text
#: ./TODO.md:8
msgid "# TODO - Onion Reveal"
msgstr ""

#. type: Plain text
#: ./TODO.md:10
msgid "## Organization"
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:21
#, markdown-text
msgid "[x] Invite Renovate bot to the project."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:21
#, markdown-text
msgid "[x] Fix `filters/fix-links-multiple-files.lua`."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:21
#, markdown-text
msgid "[x] Move project into tpo/web."
msgstr ""

#. type: Plain text
#: ./TODO.md:21
#, no-wrap
msgid ""
"* [x] Move these tasks into tickets at the [issue queue][]. Upstreamed\n"
"      into https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues/4\n"
"* [ ] Move `revealjs_tor_theme` to `tpo/web`, and update the submodule. Move "
"either one of these\n"
"      projects:\n"
"    * https://gitlab.torproject.org/rhatto/revealjs_tor_theme\n"
"    * https://gitlab.torproject.org/juga/revealjs_tor_theme (if is this one, "
"needs to ask @juga to do it)\n"
"* [ ] Create a license file for the assets.\n"
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:32
#, markdown-text
msgid "[x] Localization."
msgstr ""

#. type: Plain text
#: ./TODO.md:32
#, no-wrap
msgid ""
"* [x] QR code generation, so it's easy to embed QR codes into slides.\n"
"      Upstreamed into "
"https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues/3\n"
"* [ ] Favicon.\n"
"* [ ] Support for subsections.\n"
"* [ ] Caching:\n"
"  * [ ] Cache theme and content.\n"
"  * [ ] Cache cleaning.\n"
msgstr ""

#. type: Plain text
#: ./TODO.md:34
msgid "## Fixes"
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:54
#, markdown-text
msgid "[x] Link filter: map README.md into index.html."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:54
#, markdown-text
msgid "[x] Relative/absolute asset paths."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:54
#, markdown-text
msgid "[x] Merge requests improving juga's reveal.js theme:"
msgstr ""

#. type: Bullet: '  * '
#: ./TODO.md:54
#, markdown-text
msgid ""
"[x] Discussion: "
"https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15#note_2963883"
msgstr ""

#. type: Plain text
#: ./TODO.md:54
#, no-wrap
msgid ""
"  * [x] CSS updates:\n"
"        "
"https://gitlab.torproject.org/juga/revealjs_tor_theme/-/merge_requests/1\n"
"* [x] Improved merge request with further changes:\n"
"      "
"https://gitlab.torproject.org/juga/revealjs_tor_theme/-/merge_requests/2\n"
"  * [x] Additional CSS changes.\n"
"  * [x] README (installation instructions).\n"
"  * [x] Upstreaming [images](images).\n"
"  * [x] Pandoc template.\n"
"  * [x] New index.html.\n"
"  * [x] CI/CD.\n"
"* [x] Use upstreamed template (when the merge request above is "
"done/accepted, or use the forked version).\n"
"* [ ] Localization: use https://gitlab.torproject.org/tpo/community/l10n/ as "
"a submodule\n"
"      in an `onion-reveal` branch (make a Merge Request).\n"
"* [ ] Some strings aren't being translated.\n"
"      Reported upstream at "
"https://gitlab.torproject.org/tpo/community/l10n-for-markdown/-/issues/2\n"
msgstr ""

#. type: Plain text
#: ./TODO.md:56
msgid "## Documentation"
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:62
#, markdown-text
msgid "[ ] Repository URLs."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:62
#, markdown-text
msgid "[ ] Installation instructions."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:62
#, markdown-text
msgid "[ ] Localization instructions."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:62
#, markdown-text
msgid "[ ] Maintainer information."
msgstr ""

#. type: Bullet: '* '
#: ./TODO.md:62
#, markdown-text
msgid "[ ] More examples."
msgstr ""

#. type: Plain text
#: ./TODO.md:63
msgid "[issue queue]: https://gitlab.torproject.org/rhatto/onion-reveal/-/issues"
msgstr ""
