---
title: Onion Reveal
subtitle: Um compilador de sítio estático usando reveal.js com tematização para o Tor
author: Time de Comunidade - Projeto Tor
---

## Onion Reveal

Este é um compilador de slides usando [reveal.js][] similar ao [Onion TeX
Slim][].

Foi escrito durante um [Projeto da Hackweek do Tor][].

[reveal.js]: https://revealjs.com

[Onion TeX Slim]: https://gitlab.torproject.org/rhatto/onion-tex-slim

[Projeto da Hackweek do Tor]:
https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15

## Funcionalidades

* Cria shows de slides web com tematização do Tor
* Modular e não-intrusivo: pode ser incluído dentro de qualquer projeto.
* Suporte a localização via [gettext][].

[gettext]:
https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html

## Como funciona

Este software poderia funcionar assim

* Use um alvo Makefile ou script que atravessa uma pasta em busca de
  arquivos `.md`
* Então ele constrói os slides HTML usando pandoc e um template reveal.
* Com CI/CD para automação, numa maneira que torna fácil a integração em
  projetos existentes

[Pandoc]: https://pandoc.org

## Exemplos

* [Site do Onion
  Reveal](https://tpo.pages.torproject.net/web/onion-reveal/pt-br).
* [Amostra de apresentação](examples/sample.md).

## Repositório

O repositório do Onion Reveal está hospedado em
[https://gitlab.torproject.org/tpo/web/onion-reveal/](https://gitlab.torproject.org/tpo/web/onion-reveal/).

## Requisitos

Onion Reveal requer os seguintes softwares:

* [Git](https://git-scm.com).
* [NPM](https://www.npmjs.com).
* [Gulp](https://gulpjs.com).
* [Pandoc](https://pandoc.org).
* [rsync](https://rsync.samba.org).

Um script de instalação de exemplo e testado no [Debian][] Bookworm está
disponível em [scripts/onion-reveal-provision-build][].

[Debian]: https://www.debian.org
[texlive-latex-extra]: https://tracker.debian.org/pkg/texlive-extra
[scripts/onion-reveal-provision-build]: scripts/onion-reveal-provision-build
[po4a]: https://po4a.org

## Dependências opcionais

* [GNU Make](https://www.gnu.org/software/make) para construção usando
  Makefiles.
* [po4a][] para gerir traduções.

## Instalação

Este repositório está pronto para ser usado dentro de qualquer repositório
existente.  Ele pode ser instalado de diversas maneiras como segue.

## Forkando o projeto

Simplesmente forke este projeto e modifique o que você precisar.

## Como um submódulo Git

Você pode simplesmente colocá-lo como um repositório Git em algum lugar,
como numa pasta `vendors` dentro do seu projeto:

    mkdir -p vendor && \
    git submodule add --depth 1 \
      https://gitlab.torproject.org/tpo/web/onion-reveal vendors/onion-reveal

Você também precisa inicializar os submódulos do Onion Reveal:

    git -C vendors/onion-reveal submodule update --init --depth 1

Então use links simbólicos para referenciar todos os arquivos necessários na
pasta raíz do seu repositório.

## Copiando manualmente

Mais simples ainda, copie todos os arquivos relevantes deste repositório
para dentro do seu projeto.

## Fora do projeto

O repositório do Onion Reveal pode ser instalado em qualquer local do seu
sistema.  De fato, ele nem precisa estar armazenado dentro do seu projecto
se você não quiser. Basta que você se certifique de chamar os scripts a
partir da pasta do seu projeto ou passar os caminhos apropriados como
argumentos dos scripts.

## Outras maneiras

* `git-subtree(1)`.
* ?

## Notas de instalação

* Se você planeja usar tanto GitLab CI/CD ou GitHub Actions para construir
  sua documentação, certifique-se de ao menos copiar e adaptar os arquivos
  correspondentes do submódulo.
* Você não pode simplesmente usar links simbólicos uma vez que o submódulo
  não estará acessível quando o GitLab e o GitHub buscam por configurações
  de CI/CD or Actions.
* As configurações de CI fornecidas já cuidam da inicialização do submódulo
  em tempo de construção, mas elas precisam estar disponíveis no repositório
  principal para que possam funcionar.

## Layout do projeto

Layout de projeto de exemplo:

    * /caminho/para/o/meu/projeto
        * slides
          * README.md
          * slide-para-conferencia-1
              * slide-para-conferencia-1.md
          * slide-para-conferencia-2
              * slide-para-conferencia-2.md
        * vendors
            * onion-reveal

## Construindo o tema

Para construir o tema, siga as instruções em
[vendors/onion-reveal/vendors/revealjs_tor_theme/README][] ou use o seguinte
script, se você estiver usando um sistema do tipo [Debian][]:

    ./vendors/onion-reveal/scripts/onion-reveal-build-theme

[vendors/onion-reveal/vendors/revealjs_tor_theme/README]:

## Compilando todos os slides

Onion Reveal pode ser usado para construir sítios estáticos a partir de
qualquer conjunto de arquivos Markdown que ele encontrar, então é
recomendável rodá-lo para a pasta de slides de seu projeto.

Uma vez que todas as dependências estão instaladas e todos os arquivos
necessários estão no seu repositório, rode o script de construção:

    vendors/onion-reveal/scripts/onion-reveal-build-all slides/

Quando invocado sem argumentos, o script acima construirá slides a partir de
todos arquivos Markdown que encontrar recursivamente a partir da pasta
atual.

## Compilando um único slide

Você também pode construir um único maço de slides:

    vendors/onion-reveal/scripts/onion-reveal-build \
      slides/slide-for-conference-1/slide-for-conference-1.md

## Suporte a traduções

Onion Reveal funciona bem com localização (l10n). Fora da caixa, ele suporta
traduções através do [L10n for Markdown][]. Se você possui [po4a][]
installado, você pode usá-lo ao copiar/personalizar o arquivo
[l10n.sample][] e rodar o seguinte script toda vez que você quiser
sincronizar suas traduções:

    ./vendors/l10n-for-markdown/scripts/l10n-for-markdown-update-locales

[L10n for Markdown]:
https://gitlab.torproject.org/tpo/community/l10n-for-markdown/
[l10n.sample]:
https://gitlab.torproject.org/tpo/community/l10n-for-markdown/-/blob/main/l10n.sample

## Makefile

O Onion Reveal também vem com uma [amostra de Makefile][] que pode ser
customizada conforme suas necessidades, colocada na pasta desejada e então
usada para construir seus slides através de 

    make onion-reveal-build-all

Você também pode construir slides específicos usando `make`:

    make -C slides slides/slide-for-conference-1/slide-for-conference-1.html

[amostra de Makefile]:
https://gitlab.torproject.org/tpo/community/onion-reveal/-/blob/main/Makefile.onion-reveal

## Integração Contínua

Onion Reveal também suporta Integração Contínua (CI) através do
GitLab. Verifique o arquivo [.gitlab-ci-onion-reveal.yml][] para detalhes.

[.gitlab-ci-onion-reveal.yml]: .gitlab-ci-onion-reveal.yml

## A fazer e problemas

Verifique problemas existentes e reporte novos na [fila de tarefas][].

[fila de tarefas]:
https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues
