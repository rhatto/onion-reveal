---
title: Onion Reveal Sample
subtitle: An example
author: Someone
---

# First section

* An item.

# Another section

1. Numbered list.
2. Another item.
3. Another

# Codeblocks

```
this is a codeblock
```
