#!/usr/bin/env bash
#
# Build an Onion Reveal slide deck.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"
BASEDIR="$DIRNAME/.."
SOURCE="$1"
DEFAULT_LANG="en"
OUTPUT="${OUTPUT:-reveal}"

# Theme folder
THEME_DIR="$BASEDIR/vendors/revealjs_tor_theme"

# Slide theme
# See https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides
# Also look for variables at /usr/share/pandoc/data/templates/default.revealjs
THEME="${THEME:-tor}"

# Template
TEMPLATE="${TEMPLATE:-pandoc.template}"

# Build command
COMPILE_CMD="pandoc --lua-filter=${BASEDIR}/filters/fix-links-multiple-files.lua --template=${THEME_DIR}/templates/${TEMPLATE} -V theme=${THEME} -s --mathml -i -t revealjs --slide-level 2"

# Checks
if [ -z "$SOURCE" ]; then
  echo "usage: $BASENAME <source>"
  exit 1
elif [ ! -f "$SOURCE" ]; then
  echo "$BASENAME: file not found: $SOURCE"
  exit 1
fi

# Language detection
# This grep pattern does not work in some systems
#if echo $SOURCE | grep -q '\.[a-Z\-]*.md'; then
if [ "`echo $SOURCE | cut -d . -f 2-`" != "md" ]; then
  #LANG="$(echo $SOURCE | grep -E '\.[a-zA-Z\-]*.md')"
  #LANG="`echo $LANG | cut -d . -f 2`"
  LANG="$(echo $SOURCE | cut -d . -f 2- | sed -e 's/.md$//')"
  LANG_LOWER="`echo $LANG | tr '[:upper:]' '[:lower:]'`"
  EXT="$LANG.md"
else
  LANG="$DEFAULT_LANG"
  LANG_LOWER="`echo $LANG | tr '[:upper:]' '[:lower:]'`"
  EXT="md"
fi

# Set destination folder
DIR="`dirname $SOURCE`"
BASE="`basename $SOURCE .$EXT`"

# Fix DIR
if [ "$DIR" == "." ]; then
  DIR=""
else
  DIR="/$DIR"
fi

# Set destination file
if [ "$BASE" == "README" ]; then
  DEST_FOLDER="${OUTPUT}/${LANG_LOWER}${DIR}"
  DEST_FOLDER_RELATIVE="${LANG_LOWER}${DIR}"
  DEST_FOLDER_DEFAULT="${DIR}"
else
  DEST_FOLDER="${OUTPUT}/${LANG_LOWER}${DIR}/${BASE}"
  DEST_FOLDER_RELATIVE="${LANG_LOWER}${DIR}/${BASE}"
  DEST_FOLDER_DEFAULT="${DIR}/${BASE}"
fi

DEST="$DEST_FOLDER/index.html"
DEST_RELATIVE="$DEST_FOLDER_RELATIVE/index.html"
DEST_DEFAULT="$DEST_FOLDER_DEFAULT/index.html"

# Set base path
if [ -z "$DIR" ] && [ "$BASE" == "README" ]; then
  BASE_PATH=".."
else
  BASE_PATH="`echo $DEST_RELATIVE | sed -e 's|[^/]*|..|g' | sed -e 's|/..$||'`"
fi

# Set reveal.js location
REVEAL_LOCATION="$BASE_PATH/reveal.js"

# Sync assets
mkdir -p "$DEST_FOLDER"
rsync --quiet -av --delete \
  --exclude=node_modules --exclude='*.md' \
  $THEME_DIR/reveal.js/ \
  $OUTPUT/reveal.js/
rsync --quiet -av --delete $THEME_DIR/images/ $OUTPUT/images/

# Compile
echo "Building $SOURCE into $DEST..."
$COMPILE_CMD -V revealjs-url=$REVEAL_LOCATION -V base_path=$BASE_PATH $SOURCE -o $DEST

# Create a default version if lang is the default
if [ "$LANG" == "$DEFAULT_LANG" ]; then
  mkdir -p "$OUTPUT/$DEST_FOLDER_DEFAULT"
  # Symlink
  #( cd $OUTPUT &> /dev/null && ln -s $DEST_RELATIVE $DEST_DEFAULT )

  # Copy
  #( cd $OUTPUT &> /dev/null && cp $DEST_RELATIVE $DEST_DEFAULT )

  # Update base path
  if [ "$SOURCE" == "README.md" ]; then
    BASE_PATH="."
  else
    BASE_PATH="`echo $BASE_PATH | sed -e 's|../||'`"
  fi

  # Update reveal location
  REVEAL_LOCATION="$BASE_PATH/reveal.js"

  # Compile again
  $COMPILE_CMD  -V revealjs-url=$REVEAL_LOCATION -V base_path=$BASE_PATH $SOURCE -o $OUTPUT/$DEST_DEFAULT
fi
