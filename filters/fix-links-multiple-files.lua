-- Check https://pandoc.org/lua-filters.html
--       https://stackoverflow.com/questions/48169995/pandoc-how-to-link-to-a-section-in-another-markdown-file#48172069
--       https://www.lua.org/manual/5.4/manual.html#6.4.1
function Link (link)
  -- Use a simplified version
  --link.target = link.target:gsub('(.+)%.md%#(.+)', '%1.html#%2')
  --link.target = link.target:gsub('.+%.md%#(.+)', '#%1')
  --link.target = link.target:gsub('(.+)%.md', '%1.html')
  link.target = link.target:gsub('(.+)%.md', '%1/')
  return link
end
