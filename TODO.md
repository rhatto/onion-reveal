---
title: TODO - Onion Reveal
---

Tasks not yet moved to the [issue queue][], as sometimes editing a TODO file is way quicker.

# TODO - Onion Reveal

## Organization

* [x] Invite Renovate bot to the project.
* [x] Fix `filters/fix-links-multiple-files.lua`.
* [x] Move project into tpo/web.
* [x] Move these tasks into tickets at the [issue queue][]. Upstreamed
      into https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues/4
* [ ] Move `revealjs_tor_theme` to `tpo/web`, and update the submodule. Move either one of these
      projects:
    * https://gitlab.torproject.org/rhatto/revealjs_tor_theme
    * https://gitlab.torproject.org/juga/revealjs_tor_theme (if is this one, needs to ask @juga to do it)
* [ ] Create a license file for the assets.

## Features

* [x] Localization.
* [x] QR code generation, so it's easy to embed QR codes into slides.
      Upstreamed into https://gitlab.torproject.org/tpo/web/onion-reveal/-/issues/3
* [ ] Favicon.
* [ ] Support for subsections.
* [ ] Caching:
  * [ ] Cache theme and content.
  * [ ] Cache cleaning.

## Fixes

* [x] Link filter: map README.md into index.html.
* [x] Relative/absolute asset paths.
* [x] Merge requests improving juga's reveal.js theme:
  * [x] Discussion: https://gitlab.torproject.org/tpo/community/hackweek/-/issues/15#note_2963883
  * [x] CSS updates:
        https://gitlab.torproject.org/juga/revealjs_tor_theme/-/merge_requests/1
* [x] Improved merge request with further changes:
      https://gitlab.torproject.org/juga/revealjs_tor_theme/-/merge_requests/2
  * [x] Additional CSS changes.
  * [x] README (installation instructions).
  * [x] Upstreaming [images](images).
  * [x] Pandoc template.
  * [x] New index.html.
  * [x] CI/CD.
* [x] Use upstreamed template (when the merge request above is done/accepted, or use the forked version).
* [ ] Localization: use https://gitlab.torproject.org/tpo/community/l10n/ as a submodule
      in an `onion-reveal` branch (make a Merge Request).
* [ ] Some strings aren't being translated.
      Reported upstream at https://gitlab.torproject.org/tpo/community/l10n-for-markdown/-/issues/2

## Documentation

* [ ] Repository URLs.
* [ ] Installation instructions.
* [ ] Localization instructions.
* [ ] Maintainer information.
* [ ] More examples.

[issue queue]: https://gitlab.torproject.org/rhatto/onion-reveal/-/issues
